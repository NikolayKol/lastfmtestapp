package comlastfm.test.lastfmtestapp.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import comlastfm.test.lastfmtestapp.entities.image.Image;

/**
 * Created by nikolay on 12.09.17.
 */

public class AlbumPreInfo {

    @SerializedName("name")
    private String name;

    @SerializedName("artist")
    private String artist;

    @SerializedName("url")
    private String url;

    @SerializedName("image")
    private List<Image> images;

    @SerializedName("mbid")
    private String id;

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    public List<Image> getImages() {
        return images;
    }
}
