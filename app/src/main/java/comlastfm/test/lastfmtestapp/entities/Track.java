package comlastfm.test.lastfmtestapp.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikolay on 13.09.17.
 */

public class Track {

    @SerializedName("name")
    private String name;

    @SerializedName("duration")
    private int duration;

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }
}
