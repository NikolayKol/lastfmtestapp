package comlastfm.test.lastfmtestapp.entities;


import comlastfm.test.lastfmtestapp.entities.image.Image;
import comlastfm.test.lastfmtestapp.entities.image.Size;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nikolay on 14.09.17.
 */

public class RealmAlbumPreInfo extends RealmObject {

    @PrimaryKey
    private String id;

    private String searchQuery;

    private String name;

    private String artist;

    private String url;

    private String mbid;

    private RealmList<Image> images;

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMbid() {
        return mbid;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public RealmList<Image> getImages() {
        return images;
    }

    public String getSmallImageUrl() {

        getImages().size();

        for (Image image : getImages()) {

            Size size = Size.valueOf(image.getSize());

            if (size == Size.small) {
                return image.getUrl();
            }

        }

        return null;
    }
}
