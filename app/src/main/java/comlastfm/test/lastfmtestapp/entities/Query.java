package comlastfm.test.lastfmtestapp.entities;

import io.realm.RealmObject;

/**
 * Created by nikolay on 15.09.17.
 */

public class Query extends RealmObject {

    private String query;
    private int age;

    public Query() {
    }

    public Query(String query) {
        this.query = query;
        age = 0;
    }

    public String getQuery() {
        return query;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
