package comlastfm.test.lastfmtestapp.entities.image;

/**
 * Created by nikolay on 12.09.17.
 */

public enum Size {
    small,
    medium,
    large,
    extralarge,
    mega
}
