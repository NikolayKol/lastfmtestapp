package comlastfm.test.lastfmtestapp.entities.image;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nikolay on 12.09.17.
 */

public class Image extends RealmObject {

    @PrimaryKey
    @SerializedName("#text")
    private String url;

    @SerializedName("size")
    private String size;

    public String getUrl() {
        return url;
    }

    public String getSize() {
        return size;
    }
}
