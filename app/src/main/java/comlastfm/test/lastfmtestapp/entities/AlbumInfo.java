package comlastfm.test.lastfmtestapp.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import comlastfm.test.lastfmtestapp.entities.image.Image;
import comlastfm.test.lastfmtestapp.entities.image.Size;

/**
 * Created by nikolay on 12.09.17.
 */

public class AlbumInfo {

    @SerializedName("name")
    private String name;

    @SerializedName("artist")
    private String artist;

    @SerializedName("image")
    private List<Image> images;

    @SerializedName("tracks")
    private TrackList trackList;

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getImageUrl() {
        for (Image image : images) {

            Size size = Size.valueOf(image.getSize());

            if (size == Size.mega) {
                return image.getUrl();
            }
        }
        return null;
    }

    public List<Track> getTracks() {
        return trackList.tracks;
    }

    private static class TrackList {
        @SerializedName("track")
        private List<Track> tracks;
    }
}
