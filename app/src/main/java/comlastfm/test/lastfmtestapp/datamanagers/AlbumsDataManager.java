package comlastfm.test.lastfmtestapp.datamanagers;

/**
 * Created by nikolay on 12.09.17.
 */

public interface AlbumsDataManager {

    void requestAlbumInfo(String albumId);

    void requestSearchResults(String albumName);

    void requestRefreshSearchResults(String albumName);
}
