package comlastfm.test.lastfmtestapp.datamanagers.events;

import comlastfm.test.lastfmtestapp.entities.AlbumInfo;
import comlastfm.test.lastfmtestapp.network.exceptions.NetworkException;

/**
 * Created by nikolay on 15.09.17.
 */

public class AlbumInfoEvent {

    private AlbumInfo albumInfo;

    private NetworkException exception;

    public AlbumInfoEvent(AlbumInfo albumInfo) {
        this.albumInfo = albumInfo;
    }

    public AlbumInfoEvent(NetworkException exception) {
        this.exception = exception;
    }

    public AlbumInfo getAlbumInfo() {
        return albumInfo;
    }

    public NetworkException getException() {
        return exception;
    }

    public boolean isSuccessful() {
        return exception == null;
    }
}
