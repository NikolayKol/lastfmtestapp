package comlastfm.test.lastfmtestapp.datamanagers.events;

import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import comlastfm.test.lastfmtestapp.network.exceptions.NetworkException;
import io.realm.RealmResults;

/**
 * Created by nikolay on 17.09.17.
 */

public class SearchResultsEvent {

    private RealmResults<RealmAlbumPreInfo> albums;

    private NetworkException networkException;

    public SearchResultsEvent(RealmResults<RealmAlbumPreInfo> albums) {
        this.albums = albums;
    }

    public SearchResultsEvent(NetworkException networkException) {
        this.networkException = networkException;
    }

    public RealmResults<RealmAlbumPreInfo> getAlbums() {
        return albums;
    }

    public NetworkException getNetworkException() {
        return networkException;
    }

    public boolean isSuccess() {
        return networkException == null;
    }
}
