package comlastfm.test.lastfmtestapp.datamanagers;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import org.greenrobot.eventbus.EventBus;

import comlastfm.test.lastfmtestapp.R;
import comlastfm.test.lastfmtestapp.datamanagers.events.AlbumInfoEvent;
import comlastfm.test.lastfmtestapp.datamanagers.events.SearchResultsEvent;
import comlastfm.test.lastfmtestapp.entities.AlbumInfo;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import comlastfm.test.lastfmtestapp.network.LastFMApi;
import comlastfm.test.lastfmtestapp.network.exceptions.NetworkException;
import comlastfm.test.lastfmtestapp.network.responses.AlbumSearchResponse;
import comlastfm.test.lastfmtestapp.storage.LocalStorage;
import io.realm.RealmResults;

/**
 * Created by nikolay on 12.09.17.
 */

public class AlbumsDataManagerImpl implements AlbumsDataManager {

    private static final int RESULTS_MESSAGE = 101;
    private static final int EXCEPTION_MESSAGE = 102;
    private static final int REFRESH_MESSAGE = 103;

    private LastFMApi lastFMApi;
    private LocalStorage localStorage;
    private Handler handler;

    public AlbumsDataManagerImpl(Context context) {
        this.lastFMApi = new LastFMApi(context.getString(R.string.api_key));
        localStorage = new LocalStorage(context);
        handler = new Handler(new ApiCallback());
    }

    @Override
    public void requestSearchResults(final String albumName) {

        if (localStorage.containSearchResults(albumName)) {
            RealmResults<RealmAlbumPreInfo> albums = localStorage.getSearchResults(albumName);
            SearchResultsEvent event = new SearchResultsEvent(albums);
            EventBus.getDefault().post(event);
        } else {

            new Thread() {
                @Override
                public void run() {
                    Message message = new Message();

                    try {
                        message.obj = lastFMApi.search(albumName);
                        message.what = RESULTS_MESSAGE;
                    } catch (NetworkException n) {
                        message.obj = n;
                        message.what = EXCEPTION_MESSAGE;
                    }

                    handler.sendMessage(message);
                }
            }.start();
        }
    }

    @Override
    public void requestAlbumInfo(final String albumId) {

        new Thread() {
            @Override
            public void run() {
                AlbumInfoEvent event;

                try {
                    AlbumInfo albumInfo = lastFMApi.info(albumId);
                    event = new AlbumInfoEvent(albumInfo);
                } catch (NetworkException n) {
                    event = new AlbumInfoEvent(n);
                }

                EventBus.getDefault().post(event);
            }
        }.start();
    }

    @Override
    public void requestRefreshSearchResults(final String albumName) {

        new Thread() {
            @Override
            public void run() {
                Message message = new Message();
                try {
                    message.obj = lastFMApi.search(albumName);
                    message.what = REFRESH_MESSAGE;
                } catch (NetworkException n) {
                    message.obj = n;
                    message.what = EXCEPTION_MESSAGE;
                }

                handler.sendMessage(message);
            }
        }.start();
    }

    private class ApiCallback implements Handler.Callback {
        @Override
        public boolean handleMessage(Message message) {
            AlbumSearchResponse response;
            SearchResultsEvent resultsEvent;

            switch (message.what) {
                case REFRESH_MESSAGE:
                    response = (AlbumSearchResponse) message.obj;
                    localStorage.clearSearchResults(response.getQuery());
                    localStorage.setSearchResults(response.getQuery(), response.getAlbums());
                    resultsEvent = new SearchResultsEvent(localStorage.getSearchResults(response.getQuery()));
                    EventBus.getDefault().post(resultsEvent);
                    return true;
                case RESULTS_MESSAGE:
                    response = (AlbumSearchResponse) message.obj;
                    localStorage.setSearchResults(response.getQuery(), response.getAlbums());
                    resultsEvent = new SearchResultsEvent(localStorage.getSearchResults(response.getQuery()));
                    EventBus.getDefault().post(resultsEvent);
                    return true;
                case EXCEPTION_MESSAGE:
                    SearchResultsEvent exceptionEvent = new SearchResultsEvent((NetworkException) message.obj);
                    EventBus.getDefault().post(exceptionEvent);
                    return true;
                default:
                    return false;
            }
        }
    }
}
