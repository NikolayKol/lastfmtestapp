package comlastfm.test.lastfmtestapp.app.main_activity;

import comlastfm.test.lastfmtestapp.app.application.mvp.BasePresenter;
import comlastfm.test.lastfmtestapp.app.application.mvp.BaseView;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import io.realm.RealmResults;

/**
 * Created by nikolay on 12.09.17.
 */

public class MainActivityContract {

    interface MainView extends BaseView {

        void navigateToAlbumScreen(String albumId);

        void showRefresh(boolean show);

        void showLoadingIndicator(boolean show);

        void showAlbums(RealmResults<RealmAlbumPreInfo> albums);

        void clearAlbums();

        void showAlbumsEmpty(boolean show);

        void showNoInternetConnection(boolean show);

        void showLogo(boolean show);

        void clearScreen();
    }

    interface MainPresenter extends BasePresenter<MainView> {

        void setAlbumName(String albumName);

        void onAlbumClicked(int position);

        void onBackPressed();

        void onRefreshClicked();

        void onRetryClicked();
    }

}
