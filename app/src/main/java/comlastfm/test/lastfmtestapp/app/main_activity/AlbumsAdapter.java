package comlastfm.test.lastfmtestapp.app.main_activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import comlastfm.test.lastfmtestapp.R;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import io.realm.RealmResults;

/**
 * Created by nikolay on 12.09.17.
 */

class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.AlbumViewHolder> {

    interface AlbumClickListener {
        void onAlbumClicked(int position);
    }

    private RealmResults<RealmAlbumPreInfo> albums;
    private AlbumClickListener albumClickListener;


    void setAlbumClickListener(AlbumClickListener albumClickListener) {
        this.albumClickListener = albumClickListener;
    }

    void setAlbums(RealmResults<RealmAlbumPreInfo> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item, parent, false);
        return new AlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, int position) {
        holder.update(position);
    }

    @Override
    public int getItemCount() {
        return albums == null ? 0 : albums.size();
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.album_name_text_view)
        TextView albumNameTextView;

        @BindView(R.id.artist_name_text_view)
        TextView artistNameTextView;

        @BindView(R.id.album_cover_view)
        ImageView albumCover;

        AlbumViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void update(int position) {
            RealmAlbumPreInfo info = albums.get(position);
            albumNameTextView.setText(info.getName());
            artistNameTextView.setText(info.getArtist());

            Glide.with(itemView.getContext())
                    .load(info.getSmallImageUrl())
                    .into(albumCover);
        }

        @Override
        public void onClick(View view) {
            albumClickListener.onAlbumClicked(getAdapterPosition());
        }
    }
}
