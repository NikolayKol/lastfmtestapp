package comlastfm.test.lastfmtestapp.app.album_activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import comlastfm.test.lastfmtestapp.R;
import comlastfm.test.lastfmtestapp.app.application.Utils;
import comlastfm.test.lastfmtestapp.entities.Track;

public class AlbumActivity extends AppCompatActivity implements AlbumContract.AlbumView {

    private static final String ALBUM_ID_KEY = "albumId";

    public static void startActivity(Context context, String albumId) {
        Intent intent = new Intent(context, AlbumActivity.class);
        intent.putExtra(ALBUM_ID_KEY, albumId);
        context.startActivity(intent);
    }

    private String albumIdKey;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.album_cover_view)
    ImageView albumCoverView;

    @BindView(R.id.artist_name_text_view)
    TextView artistNameTextView;

    @BindView(R.id.album_name_text_view)
    TextView albumNameTextView;

    @BindView(R.id.tracks_container)
    LinearLayout tracksContainer;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.content_view)
    ScrollView contentView;

    @BindView(R.id.root_layout)
    ConstraintLayout rootLayout;

    @BindView(R.id.no_data_text_view)
    TextView noDataTextView;

    private AlbumContract.AlbumPresenter albumPresenter;
    private List<TrackViewHolder> trackViewHolders = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        albumPresenter = new AlbumPresenter();
        albumPresenter.setAlbumId(getIntent().getStringExtra(ALBUM_ID_KEY));
    }

    @Override
    protected void onStart() {
        super.onStart();
        albumPresenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        albumPresenter.detachView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void showNoConnectionError() {
        Snackbar
                .make(rootLayout, R.string.no_internet_connection, BaseTransientBottomBar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        albumPresenter.retryClicked();
                    }
                }).show();
    }

    @Override
    public void showImage(String url) {
        Glide.with(this).load(url).into(albumCoverView);
    }

    @Override
    public void showAlbumName(String albumName) {
        albumNameTextView.setText(albumName);
    }

    @Override
    public void showArtistName(String artistName) {
        artistNameTextView.setText(artistName);
    }

    @Override
    public void showTracks(List<Track> tracks) {
        for (Track track : tracks) {
            LayoutInflater.from(this).inflate(R.layout.track_item, tracksContainer, true);
            View view = tracksContainer.getChildAt(tracksContainer.getChildCount() - 1);
            TrackViewHolder viewHolder = new TrackViewHolder(view);
            viewHolder.setTrackName(track.getName());
            viewHolder.setTrackLength(track.getDuration());
            trackViewHolders.add(viewHolder);
        }
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showContentView(boolean show) {
        contentView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showNoData(boolean show) {
        noDataTextView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    static class TrackViewHolder {

        private View view;

        @BindView(R.id.track_name_view)
        TextView trackNameView;

        @BindView(R.id.track_length_view)
        TextView trackLengthView;

        TrackViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }

        void setTrackName(String name) {
            trackNameView.setText(name);
        }

        void setTrackLength(int secondsLength) {
            trackLengthView.setText(Utils.getLengthString(view.getContext(), secondsLength));
        }
    }
}
