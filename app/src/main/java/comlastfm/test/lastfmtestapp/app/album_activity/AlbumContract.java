package comlastfm.test.lastfmtestapp.app.album_activity;

import java.util.List;

import comlastfm.test.lastfmtestapp.app.application.mvp.BasePresenter;
import comlastfm.test.lastfmtestapp.app.application.mvp.BaseView;
import comlastfm.test.lastfmtestapp.entities.Track;

/**
 * Created by nikolay on 13.09.17.
 */

public class AlbumContract {

    interface AlbumView extends BaseView {
        void showNoConnectionError();

        void showImage(String url);

        void showAlbumName(String albumName);

        void showArtistName(String artistName);

        void showTracks(List<Track> tracks);

        void showLoadingIndicator(boolean show);

        void showContentView(boolean show);

        void showNoData(boolean show);
    }

    interface AlbumPresenter extends BasePresenter<AlbumView> {
        void setAlbumId(String albumId);

        void retryClicked();
    }

}
