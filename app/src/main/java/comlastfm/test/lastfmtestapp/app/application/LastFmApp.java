package comlastfm.test.lastfmtestapp.app.application;

import android.app.Application;

import comlastfm.test.lastfmtestapp.datamanagers.AlbumsDataManager;
import comlastfm.test.lastfmtestapp.datamanagers.AlbumsDataManagerImpl;

/**
 * Created by nikolay on 12.09.17.
 */

public class LastFmApp extends Application {

    private static LastFmApp lastFmApp;

    private AlbumsDataManager albumsDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        lastFmApp = this;
        albumsDataManager = new AlbumsDataManagerImpl(this);
    }

    public static LastFmApp get() {
        return lastFmApp;
    }

    public AlbumsDataManager getAlbumsDataManager() {
        return albumsDataManager;
    }
}
