package comlastfm.test.lastfmtestapp.app.album_activity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import comlastfm.test.lastfmtestapp.app.application.LastFmApp;
import comlastfm.test.lastfmtestapp.datamanagers.events.AlbumInfoEvent;
import comlastfm.test.lastfmtestapp.entities.AlbumInfo;

/**
 * Created by nikolay on 13.09.17.
 */

public class AlbumPresenter implements AlbumContract.AlbumPresenter {

    private AlbumContract.AlbumView view;
    private String albumId;
    private AlbumInfoEvent albumInfoEvent;

    @Override
    public void attachView(AlbumContract.AlbumView view) {
        this.view = view;
        EventBus.getDefault().register(this);
        view.showContentView(false);
        view.showLoadingIndicator(true);
        LastFmApp.get().getAlbumsDataManager().requestAlbumInfo(albumId);
    }

    @Override
    public void detachView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    @Override
    public void retryClicked() {
        view.showContentView(false);
        view.showLoadingIndicator(true);
        LastFmApp.get().getAlbumsDataManager().requestAlbumInfo(albumId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlbumInfoEvent(AlbumInfoEvent event) {
        this.albumInfoEvent = event;
        view.showLoadingIndicator(false);

        if (event.isSuccessful()) {
            AlbumInfo albumInfo = event.getAlbumInfo();
            if (albumInfo == null) {
                view.showNoData(true);
            } else {
                view.showContentView(true);
                view.showImage(albumInfo.getImageUrl());
                view.showAlbumName(albumInfo.getName());
                view.showArtistName(albumInfo.getArtist());
                view.showTracks(albumInfo.getTracks());
            }
        } else {
            view.showNoConnectionError();
        }
    }

}
