package comlastfm.test.lastfmtestapp.app.application.mvp;

/**
 * Created by nikolay on 12.09.17.
 */

public interface BaseView {
    void finishView();
}
