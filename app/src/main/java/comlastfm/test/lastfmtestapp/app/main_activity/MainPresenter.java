package comlastfm.test.lastfmtestapp.app.main_activity;

import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import comlastfm.test.lastfmtestapp.app.application.LastFmApp;
import comlastfm.test.lastfmtestapp.datamanagers.AlbumsDataManager;
import comlastfm.test.lastfmtestapp.datamanagers.events.SearchResultsEvent;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import io.realm.RealmResults;

/**
 * Created by nikolay on 12.09.17.
 */

class MainPresenter implements MainActivityContract.MainPresenter {

    private AlbumsDataManager albumsDataManager;
    private MainActivityContract.MainView view;

    private String albumName;
    private RealmResults<RealmAlbumPreInfo> albums;

    private boolean isRefreshing;
    private boolean refreshStopped;

    MainPresenter() {
        albumsDataManager = LastFmApp.get().getAlbumsDataManager();
    }

    @Override
    public void attachView(MainActivityContract.MainView view) {
        this.view = view;
        EventBus.getDefault().register(this);
    }

    @Override
    public void detachView() {
        this.view = null;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void setAlbumName(String albumName) {
        this.albumName = albumName;
        this.albums = null;
        view.clearAlbums();
        view.showLoadingIndicator(true);
        view.showLogo(false);
        view.showAlbumsEmpty(false);
        view.showNoInternetConnection(false);
        albumsDataManager.requestSearchResults(albumName);
    }

    @Override
    public void onBackPressed() {
        if (TextUtils.isEmpty(albumName)) {
            view.finishView();
        } else {

            if (isRefreshing) {
                view.showRefresh(false);
                isRefreshing = false;
                refreshStopped = true;
            }

            albumName = null;
            albums = null;
            view.clearScreen();
        }
    }

    @Override
    public void onRefreshClicked() {
        if (albums != null && !isRefreshing) {
            isRefreshing = true;
            view.showRefresh(true);
            albumsDataManager.requestRefreshSearchResults(albumName);
        } else {
            view.showRefresh(false);
        }
    }

    @Override
    public void onAlbumClicked(int position) {
        view.navigateToAlbumScreen(albums.get(position).getMbid());
    }

    @Override
    public void onRetryClicked() {
        if (albums == null) {
            view.showLoadingIndicator(true);
            albumsDataManager.requestSearchResults(albumName);
        } else {
            view.showRefresh(true);
            albumsDataManager.requestRefreshSearchResults(albumName);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSearchResults(SearchResultsEvent event) {
        isRefreshing = false;
        view.showLoadingIndicator(false);
        view.showRefresh(false);

        if (refreshStopped) {
            refreshStopped = false;
            return;
        } else if (event.isSuccess()) {
            albums = event.getAlbums();
            if (albums.size() == 0) {
                view.showAlbumsEmpty(true);
            } else {
                view.showAlbums(albums);
            }
            view.showNoInternetConnection(false);
        } else {
            view.showNoInternetConnection(true);
        }
    }
}
