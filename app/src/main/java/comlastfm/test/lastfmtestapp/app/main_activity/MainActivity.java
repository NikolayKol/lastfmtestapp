package comlastfm.test.lastfmtestapp.app.main_activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import comlastfm.test.lastfmtestapp.R;
import comlastfm.test.lastfmtestapp.app.album_activity.AlbumActivity;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity
        implements MainActivityContract.MainView,
        AlbumsAdapter.AlbumClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    private MainActivityContract.MainPresenter mainPresenter;

    @BindView(R.id.root_layout)
    ConstraintLayout rootLayout;

    @BindView(R.id.logo)
    ImageView logo;

    @BindView(R.id.search_edit_text)
    EditText searchEditText;

    @BindView(R.id.albums_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.toolbar)
    LinearLayout toolbar;

    @BindView(R.id.empty_results_view)
    TextView emptyResultsView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private AlbumsAdapter albumsAdapter;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mainPresenter = new MainPresenter();

        albumsAdapter = new AlbumsAdapter();
        albumsAdapter.setAlbumClickListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(albumsAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainPresenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mainPresenter.detachView();
    }

    @Override
    public void onBackPressed() {
        mainPresenter.onBackPressed();
    }

    @OnEditorAction(R.id.search_edit_text)
    boolean onEditorAction(TextView view, int actionId) {
        if (EditorInfo.IME_ACTION_SEARCH == actionId) {
            String query = searchEditText.getText().toString();
            if (!TextUtils.isEmpty(query)) {
                mainPresenter.setAlbumName(searchEditText.getText().toString());
            }

            hideKeyboard();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRefresh() {
        hideKeyboard();
        mainPresenter.onRefreshClicked();
    }

    @Override
    public void onAlbumClicked(int position) {
        mainPresenter.onAlbumClicked(position);
    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showAlbums(RealmResults<RealmAlbumPreInfo> albums) {
        albumsAdapter.setAlbums(albums);
    }

    @Override
    public void showNoInternetConnection(boolean show) {
        if (show) {
            snackbar = Snackbar.make(rootLayout, getString(R.string.no_internet_connection), BaseTransientBottomBar.LENGTH_INDEFINITE).setAction(getString(R.string.retry), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mainPresenter.onRetryClicked();
                }
            });
            snackbar.show();
        } else if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void showAlbumsEmpty(boolean show) {
        emptyResultsView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showRefresh(boolean show) {
        swipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void showLogo(boolean show) {
        logo.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void clearScreen() {
        searchEditText.getText().clear();
        clearAlbums();
        logo.setVisibility(View.VISIBLE);
        showAlbumsEmpty(false);
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void clearAlbums() {
        albumsAdapter.setAlbums(null);
    }

    @Override
    public void navigateToAlbumScreen(String albumId) {
        AlbumActivity.startActivity(this, albumId);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = getCurrentFocus();

        if (view == null) {
            view = new View(this);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
