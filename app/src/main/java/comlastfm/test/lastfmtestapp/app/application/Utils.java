package comlastfm.test.lastfmtestapp.app.application;

import android.content.Context;

import java.util.List;

import comlastfm.test.lastfmtestapp.R;

/**
 * Created by nikolay on 12.09.17.
 */

public class Utils {

    public static boolean equals(List list0, List list1) {
        return false;
    }

    public static String getLengthString(Context context, int length) {

        int minutes = length / 60;
        int seconds = length % 60;

        return context.getString(R.string.length_pattern, minutes, seconds);
    }

}
