package comlastfm.test.lastfmtestapp.storage;

import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import comlastfm.test.lastfmtestapp.entities.AlbumPreInfo;
import comlastfm.test.lastfmtestapp.entities.Query;
import comlastfm.test.lastfmtestapp.entities.QueryFields;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfo;
import comlastfm.test.lastfmtestapp.entities.RealmAlbumPreInfoFields;
import comlastfm.test.lastfmtestapp.entities.image.Image;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by nikolay on 14.09.17.
 */

public class LocalStorage {

    private static final String STORAGE_LOG = "STORAGE";
    private static final int MAX_NUMBER_OF_STORED_RESULTS = 10;

    public LocalStorage(Context context) {
        Realm.init(context);
    }

    public boolean containSearchResults(String searchQuery) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Query> queries = realm.where(Query.class).findAll();

        for (Query query : queries) {
            if (query.getQuery().equals(searchQuery)) {
                return true;
            }
        }

        return false;
    }

    public void setSearchResults(String searchQuery, List<AlbumPreInfo> albums) {

        Realm realm = Realm.getDefaultInstance();

        for (AlbumPreInfo albumPreInfo : albums) {
            realm.beginTransaction();

            List<Image> images = realm.copyToRealmOrUpdate(albumPreInfo.getImages());

            RealmAlbumPreInfo album;
            try {
                album = realm.createObject(RealmAlbumPreInfo.class, UUID.randomUUID().toString());
            } catch (Exception e) {
                Log.e(STORAGE_LOG, e.getMessage());
                realm.cancelTransaction();
                continue;
            }

            album.setSearchQuery(searchQuery);
            album.setName(albumPreInfo.getName());
            album.setArtist(albumPreInfo.getArtist());
            album.setUrl(albumPreInfo.getUrl());
            album.setMbid(albumPreInfo.getId());
            album.getImages().addAll(images);
            realm.commitTransaction();
        }

        setQueriesOlder(realm, searchQuery);
        clearOldQueriesAndResults(realm);
    }

    public RealmResults<RealmAlbumPreInfo> getSearchResults(String searchQuery) {
        Realm realm = Realm.getDefaultInstance();

        RealmResults<RealmAlbumPreInfo> albums = realm.where(RealmAlbumPreInfo.class)
                .equalTo(RealmAlbumPreInfoFields.SEARCH_QUERY, searchQuery)
                .findAll();

        setQueriesOlder(realm, searchQuery);

        return albums;
    }

    public void clearSearchResults(String searchQuery) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<RealmAlbumPreInfo> albums = getSearchResults(searchQuery);
        realm.beginTransaction();
        albums.deleteAllFromRealm();
        realm.commitTransaction();
        removeQuery(realm, searchQuery);
    }

    private void setQueriesOlder(Realm realm, String currentQuery) {
        RealmResults<Query> allQueries = realm.where(Query.class).findAllSorted(QueryFields.AGE, Sort.ASCENDING);

        for (Query query : allQueries) {
            realm.beginTransaction();
            if (query.getQuery().equals(currentQuery)) {
                realm.cancelTransaction();
                break;
            } else {
                query.setAge(query.getAge() + 1);
            }
            realm.commitTransaction();
        }

        Query query = realm.where(Query.class).equalTo(QueryFields.QUERY, currentQuery).findFirst();
        realm.beginTransaction();

        if (query != null) {
            query.setAge(0);
        } else {
            realm.copyToRealm(new Query(currentQuery));
        }

        realm.commitTransaction();
    }

    private void removeQuery(Realm realm, String currentQuery) {
        RealmResults<Query> allQueries = realm.where(Query.class).findAllSorted(QueryFields.AGE, Sort.DESCENDING);

        for (Query query : allQueries) {
            realm.beginTransaction();
            if (query.getQuery().equals(currentQuery)) {
                query.deleteFromRealm();
                realm.commitTransaction();
                break;
            } else {
                query.setAge(query.getAge() - 1);
            }
            realm.commitTransaction();
        }
    }

    private void clearOldQueriesAndResults(Realm realm) {
        realm.beginTransaction();

        RealmResults<Query> oldQueries = realm.where(Query.class)
                .greaterThan(QueryFields.AGE, MAX_NUMBER_OF_STORED_RESULTS)
                .findAll();

        for (Query query : oldQueries) {
            RealmResults<RealmAlbumPreInfo> oldResults = realm
                    .where(RealmAlbumPreInfo.class)
                    .equalTo(RealmAlbumPreInfoFields.SEARCH_QUERY, query.getQuery())
                    .findAll();

            for (RealmAlbumPreInfo info : oldResults) {
                info.getImages().deleteAllFromRealm();
            }

            oldResults.deleteAllFromRealm();
        }

        oldQueries.deleteAllFromRealm();

        realm.commitTransaction();
    }
}
