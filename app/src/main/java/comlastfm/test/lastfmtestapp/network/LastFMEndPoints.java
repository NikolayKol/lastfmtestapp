package comlastfm.test.lastfmtestapp.network;

import comlastfm.test.lastfmtestapp.network.responses.AlbumInfoResponse;
import comlastfm.test.lastfmtestapp.network.responses.AlbumSearchResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nikolay on 12.09.17.
 */

interface LastFMEndPoints {

    @GET(ServerTokens.SEARCH_ALBUMS_STORE)
    Call<AlbumSearchResponse> search(@Query("api_key") String apiKey, @Query("album") String albumName, @Query("limit") int limit);

    @GET(ServerTokens.ALBUM_INFO_STORE)
    Call<AlbumInfoResponse> info(@Query("api_key") String apiKey, @Query("mbid") String albumId);

}
