package comlastfm.test.lastfmtestapp.network.responses;

import com.google.gson.annotations.SerializedName;

import comlastfm.test.lastfmtestapp.entities.AlbumInfo;

/**
 * Created by nikolay on 13.09.17.
 */

public class AlbumInfoResponse {

    @SerializedName("album")
    private AlbumInfo albumInfo;

    public AlbumInfo getAlbumInfo() {
        return albumInfo;
    }
}
