package comlastfm.test.lastfmtestapp.network;

/**
 * Created by nikolay on 12.09.17.
 */

class ServerTokens {

    static final String BASE_URL = "http://ws.audioscrobbler.com/2.0/";

    static final String SEARCH_ALBUMS_STORE = "/2.0/?method=album.search&format=json";

    static final String ALBUM_INFO_STORE = "/2.0/?method=album.getinfo&format=json";

}
