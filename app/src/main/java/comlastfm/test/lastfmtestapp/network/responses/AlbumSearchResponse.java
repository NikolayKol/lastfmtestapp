package comlastfm.test.lastfmtestapp.network.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import comlastfm.test.lastfmtestapp.entities.AlbumPreInfo;

/**
 * Created by nikolay on 12.09.17.
 */

public class AlbumSearchResponse {

    @SerializedName("results")
    private SearchResults searchResults;

    public List<AlbumPreInfo> getAlbums() {
        return searchResults.getAlbums();
    }

    public String getQuery() {
        return searchResults.getQuery();
    }
}
