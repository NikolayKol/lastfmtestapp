package comlastfm.test.lastfmtestapp.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import comlastfm.test.lastfmtestapp.entities.AlbumInfo;
import comlastfm.test.lastfmtestapp.network.exceptions.NetworkException;
import comlastfm.test.lastfmtestapp.network.responses.AlbumInfoResponse;
import comlastfm.test.lastfmtestapp.network.responses.AlbumSearchResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by nikolay on 12.09.17.
 */

public class LastFMApi {

    private static final int PAGE_SIZE = 50;

    private String apiKey;
    private LastFMEndPoints lastFMEndPoints;

    public LastFMApi(String apiKey) {

        this.apiKey = apiKey;

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        Retrofit retrofit = new Retrofit
                .Builder()
                .client(client)
                .baseUrl(ServerTokens.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        lastFMEndPoints = retrofit.create(LastFMEndPoints.class);
    }

    public AlbumSearchResponse search(String albumName) throws NetworkException {
        // TODO: 17.09.17 implement pagination

        Call<AlbumSearchResponse> call = lastFMEndPoints.search(apiKey, albumName, PAGE_SIZE);

        try {
            Response<AlbumSearchResponse> response = call.execute();

            if (response.isSuccessful()) {
                return response.body();
            } else {
                throw new NetworkException(response.message());
            }
        } catch (IOException e) {
            throw new NetworkException(e);
        }
    }

    public AlbumInfo info(String albumId) throws NetworkException {

        Call<AlbumInfoResponse> call = lastFMEndPoints.info(apiKey, albumId);

        try {
            Response<AlbumInfoResponse> response = call.execute();

            if (response.isSuccessful()) {
                return response.body().getAlbumInfo();
            } else {
                throw new NetworkException(response.message());
            }
        } catch (IOException e) {
            throw new NetworkException(e);
        }
    }
}
