package comlastfm.test.lastfmtestapp.network.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import comlastfm.test.lastfmtestapp.entities.AlbumPreInfo;

/**
 * Created by nikolay on 12.09.17.
 */

class SearchResults {

    @SerializedName("opensearch:totalResults")
    private int totalResults;

    @SerializedName("opensearch:startIndex")
    private int startIndex;

    @SerializedName("albummatches")
    private AlbumMatches albumMatches;

    @SerializedName("@attr")
    private QueryHolder queryHolder;

    List<AlbumPreInfo> getAlbums() {
        return albumMatches.albumPreInfos;
    }

    String getQuery() {
        return queryHolder.query;
    }

    private static class AlbumMatches {
        @SerializedName("album")
        private List<AlbumPreInfo> albumPreInfos;
    }

    private static class QueryHolder {
        @SerializedName("for")
        private String query;
    }
}
